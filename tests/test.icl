module test

import StdEnv

import Text.GenParse
import Text.GenPrint

:: Tree a b = Tip a | Bin b (Tree a b) (Tree a b)
instance == (Tree a b) | == a & == b where
	(==) (Tip l) (Tip r) = l == r
	(==) (Bin lb ll lr) (Bin rb rl rr) = lb == rb && ll == rl && lr == rr
:: Rose a = Rose a .[Rose a]
instance == (Rose a) | == a where
	(==) (Rose la las) (Rose ra ras) = la == ra && las == ras
:: InfCons
	= :+: infixl 2 InfCons InfCons
	| :-: infixl 2 InfCons InfCons
	| :*: infixl 3 InfCons InfCons
	| :->: infixr 4 InfCons InfCons
	| U
	| I Int
instance == InfCons where
	(==) (ll :+: lr) (rl :+: rr) = ll == rl && lr == rr
	(==) (ll :-: lr) (rl :-: rr) = ll == rl && lr == rr
	(==) (ll :*: lr) (rl :*: rr) = ll == rl && lr == rr
	(==) (ll :->: lr) (rl :->: rr) = ll == rl && lr == rr
	(==) U U = True
	(==) (I l) (I r) = l == r
	(==) _ _ = False
:: Rec a b c = { rec_fst :: a, rec_snd :: b, rec_thd :: c }
instance == (Rec a b c) | == a & == b & == c where
	(==) l r = l.rec_fst == r.rec_fst && l.rec_snd == r.rec_snd && l.rec_thd == r.rec_thd
:: Color = Red | Green | Blue
instance == Color where
	(==) Red Red = True
	(==) Green Green = True
	(==) Blue Blue = True
	(==) _ _ = False
:: NewType =: NT Int
instance == NewType where
	(==) (NT l) (NT r) = l == r

instance == {a} | == a where
	(==) l r = [l\\l<-:l] == [r\\r<-:r]
instance == (?#a) | ==, UMaybe a where
	(==) l r = maybeEq l r
instance == (?^a) | == a where
	(==) l r = maybeEq l r
instance == (?a) | == a where
	(==) l r = maybeEq l r

maybeEq :: !(m a) !(m a) -> Bool | Maybe m a & == a
maybeEq ?|None ?|None = True
maybeEq (?|Just l) (?|Just r) = l == r
maybeEq _ _ = False

derive gPrint Tree, Rose, Color, InfCons, Rec, NewType
derive gParse Tree, Rose, Color, InfCons, Rec, NewType

testParsePrint =
	[ test "int positive" 1
	, test "int positive" 123
	, test "int negative" -123

	, test "real positive" 1.09
	, test "real leading zero" 0.123
	, test "real negative" -123.456
	, test "real scientific negative exponent" 1.23E-12
	, test "real scientific positive exponent" 1.23E+12
	, test "real scientific no exponent" 1.23E5

	, test "bool true" True
	, test "bool false" False

	, test "char a" 'a'
	, test "char newline" '\n'
	, test "char double quote" '"'
	, test "char single quote" '\''
	, test "string" "Hello"
	, test "string with newline" "Hello\n"
	, test "string with quotes" "Hello \"string\""

	, test "list empty" nil
	, test "list singleton int" [1]
	, test "list of ints" [1,2,3]

	, test "array empty" (arr nil)
	, test "array singleton int" (arr [1])
	, test "array of ints" (arr [1,2,3])

	, test "color" Red
	, test "color" Green
	, test "color" Blue

	, test "record of int, bool and real" {rec_fst=1, rec_snd='a', rec_thd=1.2}

	, test "tree" (Bin 'a' (Tip 1) (Bin 'b' (Tip 2) (Bin 'c' (Tip 3) (Tip 4))))
	, test "rose" (Rose 1 [Rose 2 [], Rose 3 [], Rose 4 [Rose 5 []]])

	, test "expressions u + u" (U :+: U)
	, test "expressions u + u + u" (U :+: U :+: U)
	, test "expressions u -> u -> u" (U :->: U :->: U)
	, test "expressions" (U :+: U :*: U)
	, test "expressions" (U :*: U :->: U)
	, test "expressions" (I 1 :+: I 2 :+: I 3)
	, test "expressions" (I 1 :*: I 2 :+: I 3)
	, test "expressions" (I 1 :+: I 2 :*: I 3)
	, test "expressions" (I 1 :+: I 2 :*: I 3 :+: I 4)
	, test "expressions" (I 1 :+: (I 2 :+: I 3) :+: I 4)

	, test "list of expressions" [I 1 :+: I 2 :+: I 3, I 4 :->: I 5 :->: I 6]
	, test "array of expressions" (arr [I 1 :+: I 2 :+: I 3, I 4 :->: I 5 :->: I 6])

	, test "newtype" (NT 5)
	, test "newtype" (NT 42)
	, test "record of expressions"
		{ rec_fst = I 1 :+: I 2 :+: I 3
		, rec_snd = I 4 :->: I 5 :->: I 6
		, rec_thd = I 7 :*: I 8 :+: I 9
		}

	, test "strict maybe" none
	, test "strict maybe" (?Just 42)

	, test "lazy maybe" lnone
	, test "lazy maybe" (?^Just 42)

	, test "unboxed maybe" bnone
	, test "unboxed maybe" (?#Just 42)

	, test "unit" ()
	]
where
	test :: String a -> (String, Bool) | gPrint{|*|}, gParse{|*|}, == a
	test msg x
		# printrep = printToString x
		= case parseString printrep of
			?None = (msg +++ " (parse failed: " +++ printrep +++ ")", False)
			?Just y
				| x == y = (msg, True)
				| otherwise = (msg +++ "not equal (" +++ printrep +++ ")", x == y)

	nil :: [Int]
	nil = []

	arr :: [a] -> {a}
	arr xs = {x\\x<-xs}

	none :: ?Int
	none = ?None

	lnone :: ?^Int
	lnone = ?^None

	bnone :: ?#Int
	bnone = ?#None

Start :: !*World -> ()
Start w
	| toString (ExprApp {ExprIdent "Bin", ExprInt 1}) <> "(Bin 1)" = abort "toString of :: Expr is incorrect\n"
	= case filter (not o snd) testParsePrint of
		[] = ()
		failed
			| world_to_true (snd (fclose (foldl (\io m->io <<< fst m <<< "\n") stderr failed) w))
			// Should use setReturnCode (but this can only be done once the system package is available)
			= abort "gPrint and gParse are not eachothers inverse\n"

world_to_true :: !*World -> Bool
world_to_true w = True
