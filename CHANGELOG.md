# Changelog

#### 3.2.1

- Chore: support base `3.0`.

### 3.2.0

- Feature: add `gPrint` derivation for `:: JSONNode`.

#### 3.1.1

- Fix: the `toString` implementation  for `:: Expr`.

### 3.1

- Feature: add derivation of `gPrint` for `:: Either`.

## 3.0

- Removed: undo changes made in version 2.0.
           The derivations for `gPrint` for `Map`, `Set` and `MaybeError` are
		   now a part of the containers package.

### 2.1

- Feature: add derive of `gPrint` for `FileError`.

## 2.0

- Feature: add derives of `gPrint` for `MaybeError`, `Map` and `Set`.
           (this is a major update because these derivations were already done in applications,
           breaking them)

#### 1.2.0

- Feature: Add `==` instance for `:: Expr`.

#### 1.1.1

- Chore: Support base ^1.0 and ^2.0.

### 1.1.0

- Add `gParse` (from clean platform v0.3.34) and add tests to verify they are their inverses.

## 1.0.0

- Add gPrint derives for the builtin maybe types
- Initial version, moved gPrint from clean platform v0.3.34 to seperate package.
