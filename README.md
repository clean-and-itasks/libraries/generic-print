# generic-print

This library provides basic functionality for the generic printing and parsing of datatypes.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
