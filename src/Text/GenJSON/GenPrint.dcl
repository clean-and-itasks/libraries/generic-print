definition module Text.GenJSON.GenPrint

from Text.GenPrint import generic gPrint, :: PrintState, class PrintOutput
from Text.GenJSON import :: JSONNode

derive gPrint JSONNode
