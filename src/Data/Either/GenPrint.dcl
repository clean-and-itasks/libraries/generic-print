definition module Data.Either.GenPrint

from Data.Either import :: Either

from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState

derive gPrint Either
